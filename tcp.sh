#!/bin/bash
est_con=$(netstat -at | grep 'ESTABLISHED')

if [ "$1" = '-f' ]
then
	if [ -n "$2" ]
	then
		echo $est_con>>$2
	else
		echo 'No file name'
	fi
else
	echo $est_con
fi
